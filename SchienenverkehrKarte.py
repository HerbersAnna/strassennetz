import matplotlib
import pandas
import copy

# datei einlesen und copiert, damit die originalliste nicht verändert wird
data = pandas.read_csv('stops.csv')
dataframe = data.copy()

# doppelter IDX in einer Spalte benannt und anschließend gelöscht. stop_id auch gelöscht: erstmal nicht benötigt
dataframe.set_axis(['IDX', 'stop_name','stop_id','stop_lat','stop_lon'], 
                    axis='columns', inplace=True)
del dataframe['IDX']
del dataframe['stop_id']

# dict angelegt um Karte zeichnen zukönnen
d_names = dataframe.set_index('stop_name').T.to_dict('list')
print(d_names)
print(type(d_names))
